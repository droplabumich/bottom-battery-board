EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Sphere_lib
LIBS:Bottom_Battery_Plate-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BatCont J10
U 1 1 57322269
P 3700 1900
F 0 "J10" H 3550 1900 60  0000 C CNN
F 1 "BatCont" H 3850 1900 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 3700 1900 60  0001 C CNN
F 3 "" H 3700 1900 60  0000 C CNN
	1    3700 1900
	1    0    0    -1  
$EndComp
$Comp
L BatCont J1
U 1 1 573222F6
P 2150 1900
F 0 "J1" H 2450 1900 60  0000 C CNN
F 1 "BatCont" H 2150 1900 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 2150 1900 60  0001 C CNN
F 3 "" H 2150 1900 60  0000 C CNN
	1    2150 1900
	-1   0    0    -1  
$EndComp
$Comp
L BatCont J12
U 1 1 5732270C
P 3700 2650
F 0 "J12" H 3550 2650 60  0000 C CNN
F 1 "BatCont" H 3850 2650 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 3700 2650 60  0001 C CNN
F 3 "" H 3700 2650 60  0000 C CNN
	1    3700 2650
	1    0    0    -1  
$EndComp
$Comp
L BatCont J3
U 1 1 57322712
P 2150 2650
F 0 "J3" H 2450 2650 60  0000 C CNN
F 1 "BatCont" H 2150 2650 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 2150 2650 60  0001 C CNN
F 3 "" H 2150 2650 60  0000 C CNN
	1    2150 2650
	-1   0    0    -1  
$EndComp
$Comp
L BatCont J11
U 1 1 5732281A
P 3700 2300
F 0 "J11" H 3550 2300 60  0000 C CNN
F 1 "BatCont" H 3850 2300 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 3700 2300 60  0001 C CNN
F 3 "" H 3700 2300 60  0000 C CNN
	1    3700 2300
	1    0    0    -1  
$EndComp
$Comp
L BatCont J2
U 1 1 57322820
P 2150 2300
F 0 "J2" H 2450 2300 60  0000 C CNN
F 1 "BatCont" H 2150 2300 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 2150 2300 60  0001 C CNN
F 3 "" H 2150 2300 60  0000 C CNN
	1    2150 2300
	-1   0    0    -1  
$EndComp
$Comp
L BatCont J13
U 1 1 573228A2
P 3700 3050
F 0 "J13" H 3550 3050 60  0000 C CNN
F 1 "BatCont" H 3850 3050 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 3700 3050 60  0001 C CNN
F 3 "" H 3700 3050 60  0000 C CNN
	1    3700 3050
	1    0    0    -1  
$EndComp
$Comp
L BatCont J4
U 1 1 573228A8
P 2150 3050
F 0 "J4" H 2450 3050 60  0000 C CNN
F 1 "BatCont" H 2150 3050 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 2150 3050 60  0001 C CNN
F 3 "" H 2150 3050 60  0000 C CNN
	1    2150 3050
	-1   0    0    -1  
$EndComp
$Comp
L BatCont J14
U 1 1 57322928
P 3700 3450
F 0 "J14" H 3550 3450 60  0000 C CNN
F 1 "BatCont" H 3850 3450 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 3700 3450 60  0001 C CNN
F 3 "" H 3700 3450 60  0000 C CNN
	1    3700 3450
	1    0    0    -1  
$EndComp
$Comp
L BatCont J5
U 1 1 5732292E
P 2150 3450
F 0 "J5" H 2450 3450 60  0000 C CNN
F 1 "BatCont" H 2150 3450 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 2150 3450 60  0001 C CNN
F 3 "" H 2150 3450 60  0000 C CNN
	1    2150 3450
	-1   0    0    -1  
$EndComp
$Comp
L BatCont J15
U 1 1 573229CA
P 3700 3850
F 0 "J15" H 3550 3850 60  0000 C CNN
F 1 "BatCont" H 3850 3850 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 3700 3850 60  0001 C CNN
F 3 "" H 3700 3850 60  0000 C CNN
	1    3700 3850
	1    0    0    -1  
$EndComp
$Comp
L BatCont J6
U 1 1 573229D0
P 2150 3850
F 0 "J6" H 2450 3850 60  0000 C CNN
F 1 "BatCont" H 2150 3850 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 2150 3850 60  0001 C CNN
F 3 "" H 2150 3850 60  0000 C CNN
	1    2150 3850
	-1   0    0    -1  
$EndComp
$Comp
L BatCont J16
U 1 1 57322A90
P 3700 4250
F 0 "J16" H 3550 4250 60  0000 C CNN
F 1 "BatCont" H 3850 4250 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 3700 4250 60  0001 C CNN
F 3 "" H 3700 4250 60  0000 C CNN
	1    3700 4250
	1    0    0    -1  
$EndComp
$Comp
L BatCont J7
U 1 1 57322A96
P 2150 4250
F 0 "J7" H 2450 4250 60  0000 C CNN
F 1 "BatCont" H 2150 4250 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 2150 4250 60  0001 C CNN
F 3 "" H 2150 4250 60  0000 C CNN
	1    2150 4250
	-1   0    0    -1  
$EndComp
$Comp
L BatCont J17
U 1 1 57322B5E
P 3700 4650
F 0 "J17" H 3550 4650 60  0000 C CNN
F 1 "BatCont" H 3850 4650 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 3700 4650 60  0001 C CNN
F 3 "" H 3700 4650 60  0000 C CNN
	1    3700 4650
	1    0    0    -1  
$EndComp
$Comp
L BatCont J8
U 1 1 57322B64
P 2150 4650
F 0 "J8" H 2450 4650 60  0000 C CNN
F 1 "BatCont" H 2150 4650 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 2150 4650 60  0001 C CNN
F 3 "" H 2150 4650 60  0000 C CNN
	1    2150 4650
	-1   0    0    -1  
$EndComp
$Comp
L BatCont J18
U 1 1 57322BCC
P 3700 5050
F 0 "J18" H 3550 5050 60  0000 C CNN
F 1 "BatCont" H 3850 5050 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 3700 5050 60  0001 C CNN
F 3 "" H 3700 5050 60  0000 C CNN
	1    3700 5050
	1    0    0    -1  
$EndComp
$Comp
L BatCont J9
U 1 1 57322BD2
P 2150 5050
F 0 "J9" H 2450 5050 60  0000 C CNN
F 1 "BatCont" H 2150 5050 60  0000 C CNN
F 2 "Sphere_lib_footprint:Keystone_5224_Battery_Contact" H 2150 5050 60  0001 C CNN
F 3 "" H 2150 5050 60  0000 C CNN
	1    2150 5050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3050 1750 3650 1750
Wire Wire Line
	3050 2150 3650 2150
Wire Wire Line
	3050 2500 3650 2500
Wire Wire Line
	3050 2900 3650 2900
Wire Wire Line
	3050 3300 3650 3300
Wire Wire Line
	3050 3700 3650 3700
Wire Wire Line
	3050 4100 3650 4100
Wire Wire Line
	3050 4500 3650 4500
Wire Wire Line
	3050 4900 3650 4900
$Comp
L FUSE F1
U 1 1 57601F0B
P 2800 1750
F 0 "F1" H 2900 1800 50  0000 C CNN
F 1 "FUSE" H 2700 1700 50  0000 C CNN
F 2 "Sphere_lib:poly_1812" H 2800 1750 50  0001 C CNN
F 3 "" H 2800 1750 50  0000 C CNN
	1    2800 1750
	1    0    0    -1  
$EndComp
$Comp
L FUSE F2
U 1 1 57601F71
P 2800 2150
F 0 "F2" H 2900 2200 50  0000 C CNN
F 1 "FUSE" H 2700 2100 50  0000 C CNN
F 2 "Sphere_lib:poly_1812" H 2800 2150 50  0001 C CNN
F 3 "" H 2800 2150 50  0000 C CNN
	1    2800 2150
	1    0    0    -1  
$EndComp
$Comp
L FUSE F3
U 1 1 57601FDD
P 2800 2500
F 0 "F3" H 2900 2550 50  0000 C CNN
F 1 "FUSE" H 2700 2450 50  0000 C CNN
F 2 "Sphere_lib:poly_1812" H 2800 2500 50  0001 C CNN
F 3 "" H 2800 2500 50  0000 C CNN
	1    2800 2500
	1    0    0    -1  
$EndComp
$Comp
L FUSE F4
U 1 1 57602036
P 2800 2900
F 0 "F4" H 2900 2950 50  0000 C CNN
F 1 "FUSE" H 2700 2850 50  0000 C CNN
F 2 "Sphere_lib:poly_1812" H 2800 2900 50  0001 C CNN
F 3 "" H 2800 2900 50  0000 C CNN
	1    2800 2900
	1    0    0    -1  
$EndComp
$Comp
L FUSE F5
U 1 1 57602098
P 2800 3300
F 0 "F5" H 2900 3350 50  0000 C CNN
F 1 "FUSE" H 2700 3250 50  0000 C CNN
F 2 "Sphere_lib:poly_1812" H 2800 3300 50  0001 C CNN
F 3 "" H 2800 3300 50  0000 C CNN
	1    2800 3300
	1    0    0    -1  
$EndComp
$Comp
L FUSE F6
U 1 1 576020F9
P 2800 3700
F 0 "F6" H 2900 3750 50  0000 C CNN
F 1 "FUSE" H 2700 3650 50  0000 C CNN
F 2 "Sphere_lib:poly_1812" H 2800 3700 50  0001 C CNN
F 3 "" H 2800 3700 50  0000 C CNN
	1    2800 3700
	1    0    0    -1  
$EndComp
$Comp
L FUSE F7
U 1 1 57602165
P 2800 4100
F 0 "F7" H 2900 4150 50  0000 C CNN
F 1 "FUSE" H 2700 4050 50  0000 C CNN
F 2 "Sphere_lib:poly_1812" H 2800 4100 50  0001 C CNN
F 3 "" H 2800 4100 50  0000 C CNN
	1    2800 4100
	1    0    0    -1  
$EndComp
$Comp
L FUSE F8
U 1 1 576021CC
P 2800 4500
F 0 "F8" H 2900 4550 50  0000 C CNN
F 1 "FUSE" H 2700 4450 50  0000 C CNN
F 2 "Sphere_lib:poly_1812" H 2800 4500 50  0001 C CNN
F 3 "" H 2800 4500 50  0000 C CNN
	1    2800 4500
	1    0    0    -1  
$EndComp
$Comp
L FUSE F9
U 1 1 576023BC
P 2800 4900
F 0 "F9" H 2900 4950 50  0000 C CNN
F 1 "FUSE" H 2700 4850 50  0000 C CNN
F 2 "Sphere_lib:poly_1812" H 2800 4900 50  0001 C CNN
F 3 "" H 2800 4900 50  0000 C CNN
	1    2800 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 1750 2550 1750
Wire Wire Line
	2550 2150 2200 2150
Wire Wire Line
	2200 2500 2550 2500
Wire Wire Line
	2200 2900 2550 2900
Wire Wire Line
	2200 3300 2550 3300
Wire Wire Line
	2200 3700 2550 3700
Wire Wire Line
	2200 4100 2550 4100
Wire Wire Line
	2200 4500 2550 4500
Wire Wire Line
	2200 4900 2550 4900
$EndSCHEMATC
