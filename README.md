# README #

This repository contains the design files for the Bottom Battery Board for the deep sea sphere. More information about the project can be found [here](http://www.dropsphere.net). A BOM and pdf version of the schematic can be found inside the /docs folder. 

![Main Schematic](docs/Bottom_Battery_Plate-1.png)
![PCB](docs/IMG_2166.jpg)
